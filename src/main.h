#ifndef MAIN_H
#define MAIN_H

#include <stm32f1xx_hal.h>

void Error_handler(void);
void SysClk_Setup(void);

static void GPIO_Setup(void);

#define LED_GPIO_Port GPIOC
#define LED_Pin GPIO_PIN_13

#define SWITCH_GPIO_Port GPIOB
#define SWITCH_Pin GPIO_PIN_9

void Error_handler(void) {
  __disable_irq();
  while(1) {

  }
}
#endif
